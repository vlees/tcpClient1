import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.apache.thrift.transport.TFramedTransport;

public class thread2 extends Thread {
    public static Hello.Client client;
    public static String str;
    public thread2(String str){
        this.str = str;
    }
    @Override
    public void run(){
        System.out.println("客户端2启动....");
        try {
            TTransport transport = new TFramedTransport(new TSocket("localhost", 9010, 30000));
            // 协议要和服务端一致
            TProtocol protocol = new TBinaryProtocol(transport);
            client = new Hello.Client(protocol);
            transport.open();
            send(str);
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        } catch (Exception e){

        }
        finally {
           /* if (null != transport) {
                transport.close();
            }*/
        }
    }
    public static void send(String str){
        try {
            String result = client.helloString(str);
            System.out.println(result);
        } catch (TException e ){
            e.printStackTrace();
        }

    }
}
