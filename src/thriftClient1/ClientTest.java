package thriftClient1;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.*;

import java.io.IOException;


public class ClientTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        try {
            TTransport tSockets = new TSocket("localhost",9999);
           // TTransport tSocket = new TFramedTransport(tSockets);
            ClientHandshakeService.Client client = new ClientHandshakeService.Client(new TBinaryProtocol(tSockets));
            tSockets.open();
            runMethod(tSockets);
            client.Login("client");
            //向服务端发送消息
            for (int i = 0; i < 10; ++i){
                client.HandShake();
                Thread.sleep(0);
            }
            System.in.read();
            tSockets.close();
        } catch (TTransportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void runMethod(final TTransport tSocket){
        Thread thread = new Thread(new Runnable(){
            ServerCallbackServiceImpl serverCallbackServiceImpl = new ServerCallbackServiceImpl(tSocket);
            @Override
            public void run() {
                // TODO Auto-generated method stub
                serverCallbackServiceImpl.process();
                System.out.println("进入。。。");
            }

        });
        thread.start();
    };
}
