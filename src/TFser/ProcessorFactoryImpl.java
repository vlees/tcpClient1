package TFser;


import org.apache.thrift.TProcessor;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.transport.TTransport;


public class ProcessorFactoryImpl extends TProcessorFactory {

    public ProcessorFactoryImpl(TProcessor processor) {
        super(processor);
    }

    @Override
    public TProcessor getProcessor(TTransport trans) {
        return new TFRPCService.Processor(new TFSerImpl(trans));
    }
}
