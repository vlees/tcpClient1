package TFser;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.*;

public class TFser {
    public static void main(String[] args) {
        try {
//            //非阻塞式服务
//            TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(9999);
//            TNonblockingServer.Args targs = new TNonblockingServer.Args(serverTransport);
//            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
//            targs.protocolFactory(new TBinaryProtocol.Factory());
//            targs.transportFactory(new TFramedTransport.Factory());
//            targs.processorFactory(tProcessorFactory);
//            TNonblockingServer server = new TNonblockingServer(targs);
//            System.out.println("非阻塞式服务启动");
//            server.serve();

            TServerSocket tServerSocket = new TServerSocket(9999);
            TThreadPoolServer.Args targs = new TThreadPoolServer.Args(tServerSocket);
            TBinaryProtocol.Factory factory = new TBinaryProtocol.Factory();
            //获取processFactory
            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
            targs.protocolFactory(factory);
            targs.processorFactory(tProcessorFactory);
            targs.maxWorkerThreads(10);
            TThreadPoolServer tThreadPoolServer = new TThreadPoolServer(targs);
            System.out.println("start server...");
            tThreadPoolServer.serve();


    } catch (TTransportException e) {
        e.printStackTrace();
    }
    }

}
