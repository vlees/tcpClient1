package TFClient;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;

import java.io.IOException;

public class Test {
    public static void main(String[] args) {
        try {
           // TTransport socket = new TSocket("localhost",9999);
            TTransport socket = new TSocket("10.9.10.56",9090);
            TTransport transport = new TFramedTransport(socket);
            TProtocol protocol = new TBinaryProtocol(transport);
            TFRPCService.Client client = new TFRPCService.Client(protocol);
            transport.open();
            runMethod(socket);
            // client.Login("client");
            //向服务端发送消息
            for (int i = 0; i < 10; ++i){
                System.out.println("发送请求");
                client.Request("客户端请求" + i);
                Thread.sleep(1000);
            }
            System.in.read();
            transport.close();
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public static void runMethod(final TTransport tSocket){
        Thread thread = new Thread(new Runnable(){
            TFClientImpl serverCallbackServiceImpl = new TFClientImpl(tSocket);
            @Override
            public void run() {
                // TODO Auto-generated method stub
                serverCallbackServiceImpl.process();
            }

        });
        thread.start();
    };
}
