package TFClient;

import org.apache.thrift.TException;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TTransport;

public class TFClientImpl implements TFRPCService.Iface {
    protected int nMsgCount = 0;
    protected TTransport socket;
    protected TProcessor processor;
    public TFClientImpl(TTransport socket){
        this.socket = socket;
    }

    @Override
    public void Request(String msg) throws TException {
        System.out.println("服务端" + msg);
    }

    @Override
    public String Reply() throws TException {
        return null;
    }
    public void process(){
        processor = new TFRPCService.Processor<TFRPCService.Iface>(this);
        TBinaryProtocol protocol = new TBinaryProtocol(socket);
        while (true)
        {
            try
            {
                //TProcessor，负责调用用户定义的服务接口，从一个接口读入数据，写入一个输出接口
                while (processor.process(protocol, protocol)){
                    //阻塞式方法,不需要内容
                    System.out.println("走阻塞式方法");
                    //关闭socket
                    //socket.close();
                }
                //connection lost, return
                return;
            }catch (TException e){
                System.out.println("连接已断开...");
                e.printStackTrace();
                return;
            }
        }
    }
}
