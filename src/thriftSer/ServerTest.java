package thriftSer;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.*;
import org.apache.thrift.transport.*;


public class ServerTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            TServerSocket tServerSocket = new TServerSocket(9999);
            TThreadPoolServer.Args targs = new TThreadPoolServer.Args(tServerSocket);
            TBinaryProtocol.Factory factory = new TBinaryProtocol.Factory();
            //获取processFactory
            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
            targs.protocolFactory(factory);
            targs.processorFactory(tProcessorFactory);
            TThreadPoolServer tThreadPoolServer = new TThreadPoolServer(targs);
            System.out.println("start server...");
            tThreadPoolServer.serve();

//            //非阻塞式服务
//            TNonblockingServerTransport tServerSocket = new TNonblockingServerSocket(9999);
//            THsHaServer.Args targs = new THsHaServer.Args(tServerSocket);
//            TBinaryProtocol.Factory factory = new TBinaryProtocol.Factory();
//            //获取processFactory
//            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
//            targs.protocolFactory(factory);
//            targs.transportFactory(new TFramedTransport.Factory());
//            targs.processorFactory(tProcessorFactory);
//            TServer tThreadPoolServer = new THsHaServer(targs);
//            System.out.println("非阻塞式服务...");
//            tThreadPoolServer.serve();


//            //非阻塞式服务
//            TNonblockingServerTransport tServerSocket = new TNonblockingServerSocket(9999);
//            TThreadedSelectorServer.Args targs = new TThreadedSelectorServer.Args(tServerSocket);
//            //获取processFactory
//            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
//            targs.protocolFactory(new TBinaryProtocol.Factory());
//            //targs.transportFactory(new TFramedTransport.Factory());
//            targs.processorFactory(tProcessorFactory);
//            TThreadedSelectorServer tThreadPoolServer = new TThreadedSelectorServer(targs);
//            System.out.println("非阻塞式服务...");
//            tThreadPoolServer.serve();

//            //非阻塞式服务
//            TNonblockingServerTransport tServerSocket = new TNonblockingServerSocket(9999);
//            TThreadedSelectorServer.Args targs = new TThreadedSelectorServer.Args(tServerSocket);
//            //获取processFactory
//            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
//            targs.protocolFactory(new TBinaryProtocol.Factory());
//            //targs.transportFactory(new TFramedTransport.Factory());
//            targs.processorFactory(tProcessorFactory);
//            TNonblockingServer tThreadPoolServer = new TNonblockingServer(targs);
//            System.out.println("非阻塞式服务...");
//            tThreadPoolServer.serve();


        } catch (TTransportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
