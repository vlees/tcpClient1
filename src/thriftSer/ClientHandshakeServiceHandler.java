package thriftSer;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TTransport;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ClientHandshakeServiceHandler implements ClientHandshakeService.Iface {
    public ClientHandshakeServiceHandler(TTransport trans) {
        client = new ServerCallbackService.Client(new TBinaryProtocol(trans));
    }

    //当前已登录的连接
    public static ConcurrentHashMap<String, ServerCallbackService.Client> clientMap = new ConcurrentHashMap<String, ServerCallbackService.Client>();
    public String name;
    public boolean isLogin;
    public ServerCallbackService.Client client;
    protected boolean stopThread = false;
    protected Thread threadCallback = null;

    @Override
    public void HandShake() throws TException {
        Date nowtime = new Date();
        System.out.println("HandShake：" + client.toString() + "时间：" + nowtime);
        if (!isLogin) {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            } // 秒
            client.Push("服务端：请先登录");
        } else {
//            try {
//                TimeUnit.SECONDS.sleep(5);
//            } catch (InterruptedException e1) {
//                e1.printStackTrace();
//            } // 秒
            client.Push("服务端：请求成功。" + nowtime);
        }
        StartThread();
    }

    @Override
    public void Login(String name) throws TException {
        this.isLogin = true;
        client.Push("ser" + name + "ok");
        clientMap.put(name, client);
        client.Push("服务端：登录成功。");
        client.Push("");
        //StartThread();
    }

    @Override
    public void SendHeart(String heart) throws TException {

    }

    //开始线程
    public void StartThread() {
        System.out.println("开启线程。");
        System.out.println("当前总连接数：" + clientMap.size());
        if (threadCallback == null) {
            stopThread = false;
            threadCallback = new Thread(new CallbackThread());
            threadCallback.start();
        }
    }

    //停止线程
    public void StopThread() {
        System.out.println("停止线程。");
        stopThread = true;
        if (threadCallback != null) {
            try {
                threadCallback.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            threadCallback = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        StopThread();
        super.finalize();
    }

    class CallbackThread implements Runnable {
        public void run() {

            while (true) {
                if (stopThread) {
                    break;
                }
                try {
                    client.Push("服务端：" + client.toString());

                    Thread.sleep(0);
                } catch (TException | InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return;
                }
            }
        }
    }
}
