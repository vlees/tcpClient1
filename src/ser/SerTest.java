package ser;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.*;
import org.apache.thrift.transport.*;



public class SerTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {

//            //非阻塞式服务
//            TNonblockingServerTransport tServerSocket = new TNonblockingServerSocket(9999);
//            TThreadedSelectorServer.Args targs = new TThreadedSelectorServer.Args(tServerSocket);
//            //获取processFactory
//            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
//            targs.protocolFactory(new TBinaryProtocol.Factory());
//            //targs.transportFactory(new TFramedTransport.Factory());
//            targs.processorFactory(tProcessorFactory);
//            TServer tThreadPoolServer = new TThreadedSelectorServer(targs);
//            System.out.println("非阻塞式服务...");
//            tThreadPoolServer.serve();

            //非阻塞式服务
            TNonblockingServerTransport serverTransport = new TNonblockingServerSocket(9999);
            TNonblockingServer.Args targs = new TNonblockingServer.Args(serverTransport);
            TProcessorFactory tProcessorFactory = new ProcessorFactoryImpl(null);
            targs.protocolFactory(new TBinaryProtocol.Factory());
            targs.transportFactory(new TFramedTransport.Factory());
            targs.processorFactory(tProcessorFactory);
            TNonblockingServer server = new TNonblockingServer(targs);
            System.out.println("非阻塞式服务...");
            server.serve();

        } catch (TTransportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}


