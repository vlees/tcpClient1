package ser;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.transport.TTransport;

public class SerImpl implements CommonManageService.Iface {
    public SerImpl(TTransport trans) {
        //System.out.println("有链接介入");
        client = new CommonManageService.Client(new TBinaryProtocol(trans));
    }
    public final CommonManageService.Client client;
    protected boolean stopThread = false;
    protected Thread threadCallback = null;

    @Override
    public void ServiceRequest(String reqInfo) throws TException {
        System.out.println("客户端请求" + reqInfo);
        client.ServiceReply("服务端回复1");
      //  client.ServiceRequest("1");
      //  client.ServiceReqAndReply("2");
       // StartThread();
    }

    @Override
    public void ServiceReply(String repInfo) throws TException {
        System.out.println("客户端请求" + repInfo);
    }

    @Override
    public String ServiceReqAndReply(String reqInfo) throws TException {
        return "ok";
    }

    //开始线程
    public void StartThread() {
        System.out.println("开启线程。");
      // System.out.println("当前总连接数：" + clientMap.size());
        if (threadCallback == null) {
            stopThread = false;
            threadCallback = new Thread(new CallbackThread());
            threadCallback.start();
        }
    }

    //停止线程
    public void StopThread() {
        System.out.println("停止线程。");
        stopThread = true;
        if (threadCallback != null) {
            try {
                threadCallback.join();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            threadCallback = null;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        StopThread();
        super.finalize();
    }

    class CallbackThread implements Runnable {
        public void run() {

            while (true) {
                if (stopThread) {
                    break;
                }
                try {
                    client.ServiceRequest("服务端：" + client.toString());

                    Thread.sleep(1000);
                } catch (TException | InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return;
                }
            }
        }
    }
}
