


import org.apache.thrift.async.AsyncMethodCallback;



/**
 *  */
public class MyCallback implements AsyncMethodCallback<String> {

    // 返回结果
    @Override
    public void onComplete(String response) {
        System.out.println("onComplete");
        try {
            System.out.println(response.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }

    // 返回异常
    @Override
    public void onError(Exception exception) {
        System.out.println("onError");
    }

}