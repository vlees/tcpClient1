package client;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import thriftClient1.ServerCallbackServiceImpl;

import java.io.IOException;

public class CltTest {

    public static void main(String[] args) {
        try {
            TTransport socket = new TSocket("localhost",9999);
            TTransport transport = new TFramedTransport(socket);
            TProtocol protocol = new TBinaryProtocol(transport);
            CommonManageService.Client client = new CommonManageService.Client(protocol);
            transport.open();
            runMethod(socket);
           // client.Login("client");
            //向服务端发送消息
            for (int i = 0; i < 10; ++i){
                System.out.println("发送请求");
                client.ServiceRequest("客户端请求" + i);
                Thread.sleep(2000);
            }
            System.in.read();
            transport.close();
        } catch (TTransportException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (TException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static void runMethod(final TTransport tSocket){
        Thread thread = new Thread(new Runnable(){
            SerImpl serverCallbackServiceImpl = new SerImpl(tSocket);
            @Override
            public void run() {
                // TODO Auto-generated method stub
                serverCallbackServiceImpl.process();
            }

        });
        thread.start();
    };
}
