
import org.apache.thrift.TException;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.*;

import java.util.Date;

/**
 * @author yogo.wang
 * @date 2017/02/21-下午2:35.
 */
public class Client {
    public static Hello.Client client;
    public static void main(String[] args) throws Exception {

       // nonblockingSocket();
        //nonblockingSocket();
        asyncClient();
        asyncClient();

    }
    public  void test(){
        System.out.println("客户端启动....");
        try {
            TTransport transport = new TFramedTransport(new TSocket("localhost", 9010, 30000));
            // 协议要和服务端一致
            TProtocol protocol = new TBinaryProtocol(transport);
            client = new Hello.Client(protocol);
            transport.open();
          /*  while(true){
                send("test111111111");
            }*/
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        } finally {
           /* if (null != transport) {
                transport.close();
            }*/
        }
        long startTime=System.currentTimeMillis();
        int m = 0;
        int j = 0;
        int k = 0;
        int a = 0;
        while (m < 1000000000) {
            m++;
            while(j < 1000000000) {
                j++;
                while(k < 1000000000) {
                    k++;
                    while(a < 1000000000) {
                        a++;
                    }
                }
            }
        }
        long endTime=System.currentTimeMillis();
        System.out.println("当前程序耗时："+(endTime-startTime)+"ms");
        //new thread2().start();
      /*  thrend1 th1 = new thrend1(client);
        thrend1 th2 = new thrend1(client);*/
        thrend1 th1 = new thrend1(client,"测试1--" + "--");
        th1.start();
        thrend1 th2 = new thrend1(client,"测试1--" + "--");
        th2.start();

        int i = 0;
        while(i < 10){
            //  th1.send(thrend1.getClient(), "测试1--" + i + "--");
            //    th2.send(thrend1.getClient(), "测试2--" + i + "--");

           /* thrend1 th2 = new thrend1(client,"测试2--" + i + "--");
            th2.start();*/
            i++;
        }
    }
    public void start(){
        System.out.println("客户端启动....");
        TTransport transport = null;
        try {
            transport = new TSocket("localhost", 9010, 30000);
            // 协议要和服务端一致
            TProtocol protocol = new TBinaryProtocol(transport);
            client = new Hello.Client(protocol);
            transport.open();
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        } finally {
            if (null != transport) {
                transport.close();
            }
        }
    }
    public static void send(String str){
        try {
            String result = client.helloString(str);
            System.out.println(result);
        } catch (TException e ){
            e.printStackTrace();
        }
    }

    private static void nonblockingSocket() throws Exception {
        TTransport transport = new TFramedTransport(new TSocket("localhost", 9010));
        TProtocol protocol = new TBinaryProtocol(transport);
        Hello.Client client = new Hello.Client(protocol);
        transport.open();
        int i = 5;
        while (i > 0) {
            System.out.println("client调用返回：" + client.helloString("张三" + i));
            i--;
        }
        transport.close();
    }
    private static void asyncClient() throws Exception {
        TAsyncClientManager clientManager = new TAsyncClientManager();
        TNonblockingTransport transport = new TNonblockingSocket("localhost", 9010);
        TBinaryProtocol.Factory factory = new TBinaryProtocol.Factory();
        Hello.AsyncClient asyncClient = new Hello.AsyncClient(factory, clientManager, transport);
        System.out.println("Client calls .....");
        MyCallback callBack = new MyCallback();
        asyncClient.helloString("李四1" , callBack);
      /*  asyncClient.helloString("李四2" , callBack);
        asyncClient.helloString("李四3" , callBack);*/
        while (true) {
            Thread.sleep(1);
        }
        //System.out.println(((Hello.AsyncClient.helloString_call) res).getResult());
    }
}